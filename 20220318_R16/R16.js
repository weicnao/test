const roundLimit = 3;
let questionnaireAccessor;
var generalReportDao;
let logger;
var reportTool;

const init = function(_settings, _logger, _rpMap) {
    logger = _logger;
    questionnaireAccessor = _settings.lib.tableHelper.questionnaireAccessor;
    generalReportDao = _settings.lib.reportDao.generalReports;
    reportTool = _settings.lib.reportTool;
    logger.info(__filename + ' loaded.');
};


/*                arg.rData.datas = [
                    ['單位', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度'],
                    ['單位', '服務人數調查', '開啟滿意度調查總數', '開啟滿意度調查%', '完成滿意度調查總數', '完成滿意度調查%', '平均分數', '非常滿意100分', '滿意80分', '普通60分', '不滿意40分', '非常不滿意20分'],
                    ['智能客服', 28493, 1196, 0.042, 1056, 0.883, 98.9, 963, 53, 37, 1, 2],
                    [null, null, null, null, null, null, null, 0.912, 0.05, 0.035, 0.001, 0.002],
                    [],
                    [null,'客戶推薦意願(NPS)', '客戶推薦意願(NPS)', '客戶推薦意願(NPS)', '客戶推薦意願(NPS)', '客戶推薦意願(NPS)'],
                    [null,'完成調查總數', 'NPS分數', '推薦型佔比', '被動型佔比', '批評型佔比'],
                    [null, 5678, 64.3, 0.712, 0.219, 0.069]
                ];*/
const getData = (arg) => {
    return new Promise(async (ok, reject) => {
        arg.options.filter['channelType'] = 2;
        var roundPer = Math.pow(10, roundLimit);
        var roundAvg = Math.pow(10, 1);
        //init
        var rData = {
            datas: [
                ['單位', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度', '智能客服整體滿意度'],
                ['單位', '服務人數調查', '開啟滿意度調查總數', '開啟滿意度調查%', '完成滿意度調查總數', '完成滿意度調查%', '平均分數', '非常滿意100分', '滿意80分', '普通60分', '不滿意40分', '非常不滿意20分'],
                ['智能客服', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [null, null, null, null, null, null, null, 0, 0, 0, 0, 0],
                [],
                [null, '客戶推薦意願(NPS)', '客戶推薦意願(NPS)', '客戶推薦意願(NPS)', '客戶推薦意願(NPS)', '客戶推薦意願(NPS)'],
                [null, '完成調查總數', 'NPS分數', '推薦型佔比', '被動型佔比', '批評型佔比'],
                [null, 0, 0, 0, 0, 0]
            ],
            additional: arg.options.filter,
            customStyles: {
                rowNumMin: 4,
                defaults: {
                    alignment: {
                        vertical: 'middle',
                        horizontal: 'center'
                    },
                    border: {
                        top: {
                            style: 'thin'
                        },
                        left: {
                            style: 'thin'
                        },
                        bottom: {
                            style: 'thin'
                        },
                        right: {
                            style: 'thin'
                        }
                    }

                },
                mergeCellsList: ['A4:A5', 'B4:L4', 'A6:A7', 'B6:B7', 'C6:C7', 'D6:D7', 'E6:E7', 'F6:F7', 'G6:G7', 'M6:M7', 'N6:N7', 'O6:O7', 'P6:P7', 'Q6:Q7', 'B9:F9'],
                //深藍底 黃字 18px 標楷體 粗體
                sty1: {
                    font: {
                        name: '標楷體',
                        size: 18,
                        bold: true,
                        color: {
                            argb: 'FFFF00'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: {
                            argb: '1F497D'
                        }
                    }
                },
                //深藍底 白字 14px 標楷體 粗體
                sty2: {
                    font: {
                        name: '標楷體',
                        size: 14,
                        bold: true,
                        color: {
                            argb: 'FFFFFF'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: {
                            argb: '1F497D'
                        }
                    }
                },
                //淺藍底 白字 14px 標楷體 粗體
                sty3: {
                    font: {
                        name: '標楷體',
                        size: 14,
                        bold: true,
                        color: {
                            argb: 'FFFFFF'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: {
                            argb: '538DD5'
                        }
                    }
                },
                //白底 黑字 14px Calibri 不粗體
                sty4: {
                    font: {
                        name: 'Calibri',
                        size: 14,
                        bold: false,
                        color: {
                            argb: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: {
                            argb: 'FFFFFF'
                        }
                    },
                    numFmt: '#,##0'
                },
                //粉紅底 黑字 14px Calibri 不粗體 numFmt:0.0%
                sty5: {
                    font: {
                        name: 'Calibri',
                        size: 14,
                        bold: false,
                        color: {
                            argb: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: {
                            argb: 'DA9694'
                        }
                    },
                    numFmt: '0.0%'
                },
                //淺黃底 紅字 14px Calibri 不粗體
                sty6: {
                    font: {
                        name: 'Calibri',
                        size: 14,
                        bold: false,
                        color: {
                            argb: 'FF0000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: {
                            argb: 'FFF2CC'
                        }
                    }
                },
                //白底 黑字 14px Calibri 不粗體 numFmt:0.0%
                sty7: {
                    font: {
                        name: 'Calibri',
                        size: 14,
                        bold: false,
                        color: {
                            argb: '000000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: {
                            argb: 'FFFFFF'
                        }
                    },
                    numFmt: '0.0%'
                },  
                //淺黃底 紅字 14px Calibri 不粗體
                sty8: {
                    font: {
                        name: 'Calibri',
                        size: 14,
                        bold: false,
                        color: {
                            argb: 'FF0000'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: {
                            argb: 'FFF2CC'
                        }
                    },
                    numFmt: '0.0'
                },
                stylesMapping: [
                    ['sty2', 'sty1', 'sty1', 'sty1', 'sty1', 'sty1', 'sty1', 'sty1', 'sty1', 'sty1', 'sty1', 'sty1'],
                    ['sty2', 'sty2', 'sty2', 'sty2', 'sty2', 'sty2', 'sty2', 'sty3', 'sty3', 'sty3', 'sty3', 'sty3'],
                    ['sty2', 'sty4', 'sty4', 'sty5', 'sty4', 'sty5', 'sty6', 'sty4', 'sty4', 'sty4', 'sty4', 'sty4'],
                    ['sty2', 'sty4', 'sty4', 'sty5', 'sty4', 'sty5', 'sty6', 'sty7', 'sty7', 'sty7', 'sty7', 'sty7'],
                    [],
                    ['sty4', 'sty1', 'sty1', 'sty1', 'sty1', 'sty1'],
                    ['sty4', 'sty2', 'sty2', 'sty3', 'sty3', 'sty3'],
                    ['sty4', 'sty4', 'sty8', 'sty7', 'sty7', 'sty7']
                ]

            }
        }
        //取得使用人數

        var conUsersData = await generalReportDao.getConversationUsers(arg.options.filter);
        var webUsersCount = 0;
        if (conUsersData.recordSet && conUsersData.recordSet.length > 0) {
            webUsersCount = conUsersData.recordSet.reduce((acc, cur) => {
                var channelType = reportTool.convertChannel(cur['ChannelType']);
                if (channelType === 'web') {
                    acc = acc + cur['UserCount'];
                }
                return acc;
            }, webUsersCount);
        }

        questionnaireAccessor.getSatisfactionSurveyData(arg.options.filter).then((data) => {
            if (data.recordSet && data.recordSet.length > 0) {
                rData.additional = arg.options.filter; 
                var recordSet = data.recordSet;
                //完成滿意度調查總數
                var recordResult = recordSet.reduce((acc, cur) => {
                    var score = parseInt(cur['SCORE']);
                    if (cur['CATE'] === 'SATISFACTION') {
                        acc['satSumCount'] = acc['satSumCount'] + cur['COUNT'];
                        acc['satSumScore'] += score * cur['COUNT'];
                        acc[cur['SCORE']] = cur['COUNT'];
                    } else if (cur['CATE'] === 'RECOMMENDATION') {
                        acc['recSumCount'] = acc['recSumCount'] + cur['COUNT'];
                        acc['recSumScore'] += score * cur['COUNT'];;
                        if ([0, 1, 2, 3, 4, 5, 6].includes(score)) {
                            //批評
                            acc['recGrpLow'] += cur['COUNT'];
                        } else if ([7, 8].includes(score)) {
                            //被動
                            acc['recGrpMid'] += cur['COUNT'];
                        } else {
                            //推薦
                            acc['recGrpHigh'] += cur['COUNT'];

                        }
                    } else {
                        //ALL
                        acc['showSat'] += cur['COUNT'];

                    }
                    return acc;
                }, {
                    showSat: 0,
                    satSumCount: 0,
                    satSumScore: 0,
                    recSumCount: 0,
                    recSumScore: 0,
                    recGrpHigh: 0,
                    recGrpMid: 0,
                    recGrpLow: 0
                });
            }

            rData.datas[2] = ['智能客服', webUsersCount, recordResult['showSat'],
                webUsersCount === 0 ? 0 : (Math.round(recordResult['showSat'] * roundPer / webUsersCount) / roundPer), //開啟滿意度調查%
                recordResult['satSumCount'], //完成滿意度調查總數
                recordResult['showSat'] === 0 ? 0 : (Math.round(recordResult['satSumCount'] * roundPer / recordResult['showSat']) / roundPer), //完成滿意度調查%
                recordResult['satSumCount'] === 0 ? 0 : (Math.round(recordResult['satSumScore'] * roundAvg / recordResult['satSumCount']) / roundAvg), //平均分數
                recordResult['100'] || 0, //非常滿意100分
                recordResult['80'] || 0, //滿意80分
                recordResult['60'] || 0, //普通60分
                recordResult['40'] || 0, //不滿意40分
                recordResult['20'] || 0 //非常不滿意20分
            ];
            if(recordResult['satSumCount']!==0){
                rData.datas[3] = [null, null, null, null, null, null, null,
                    Math.round((recordResult['100'] || 0) * roundPer / recordResult['satSumCount']) / roundPer, //非常滿意100分 %
                    Math.round((recordResult['80'] || 0) * roundPer / recordResult['satSumCount']) / roundPer, //滿意80分
                    Math.round((recordResult['60'] || 0) * roundPer / recordResult['satSumCount']) / roundPer, //普通60分
                    Math.round((recordResult['40'] || 0) * roundPer / recordResult['satSumCount']) / roundPer, //不滿意40分
                    Math.round((recordResult['20'] || 0) * roundPer / recordResult['satSumCount']) / roundPer //非常不滿意20分
                ];
            }
            if(recordResult['recSumCount']!==0){
                var recommendation = Math.round(recordResult['recGrpHigh'] * roundPer / recordResult['recSumCount']) / roundPer;
                var criticism = Math.round(recordResult['recGrpLow'] * roundPer / recordResult['recSumCount']) / roundPer;
                rData.datas[7] = [null,
                    recordResult['recSumCount'], //完成調查總數
                    (recommendation - criticism) * 100, //NPS分數
                    recommendation, //推薦型佔比
                    Math.round(recordResult['recGrpMid'] * roundPer / recordResult['recSumCount']) / roundPer, //被動型佔比
                    criticism //批評型佔比
                ];
            }
            
            ok(rData);
        });

    });
};

module.exports = {
    init,
    getData
};